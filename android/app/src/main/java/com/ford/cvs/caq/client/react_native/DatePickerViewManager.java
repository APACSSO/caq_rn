package com.ford.cvs.caq.client.react_native;

import android.support.annotation.Nullable;
import android.widget.DatePicker;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 2017/3/8.
 */

public class DatePickerViewManager extends SimpleViewManager<DatePicker> {

    private static final String REACT_CLASS = "RCTDatePickerView";

    private final int DATE_PICKER_START = 1;
    private final int DATE_PICKER_END = 2;
//    private String startDate = "";
//    private String endDate = "";
//    private int startOrEnd = 0;

    private ThemedReactContext reactContext;

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected DatePicker createViewInstance(ThemedReactContext reactContext) {
        this.reactContext = reactContext;
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePicker datePicker = new DatePicker(reactContext, null, com.facebook.react.R.style.Theme_AppCompat_Light);
        datePicker.updateDate(year, month, day);
        datePicker.setCalendarViewShown(false);
        return datePicker;
    }

    private Date parseDate(String dateStr){
        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd");
        Date date = null;
        try {
            date = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

//    private boolean checkProp(){
//        if(TextUtils.isEmpty(startDate)) return false;
//        if(TextUtils.isEmpty(endDate)) return false;
//        if(startOrEnd == 0) return false;
//        return true;
//    }

    private void showDatePicker(DatePicker datePicker, int datePickerType, Date startDate, Date endDate){
        if(startDate == null && endDate == null) return;
        Calendar calendar = Calendar.getInstance();
        if(datePickerType == DATE_PICKER_START){
            datePicker.setMinDate(new Date(0).getTime());
            datePicker.setMaxDate(endDate.getTime());
            calendar.setTime(startDate);
        }else if(datePickerType == DATE_PICKER_END){
            datePicker.setMinDate(startDate.getTime());
            datePicker.setMaxDate(System.currentTimeMillis());
            calendar.setTime(endDate);
        }
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePicker.updateDate(year, month, day);
    }

//    @ReactProp(name = "startDate")
//    public void setStartDate(DatePicker view, String startDate) {
//        this.startDate = startDate;
//        Log.i("*********************", startDate);
//        if(checkProp()) {
//            showDatePicker(view, DATE_PICKER_END, parseDate(this.startDate), parseDate(this.endDate));
//        }
//    }
//
//    @ReactProp(name = "startDate")
//    public void setEndDate(DatePicker view, String endDate) {
//        this.endDate = endDate;
//        Log.i("*********************", endDate);
//        if(checkProp()) {
//            showDatePicker(view, DATE_PICKER_END, parseDate(this.startDate), parseDate(this.endDate));
//        }
//    }
//
//    @ReactProp(name = "startOrEnd", defaultInt = 0)
//    public void setStartOrEnd(DatePicker view, int startOrEnd) {
//        this.startOrEnd = startOrEnd;
//        if(checkProp()) {
//            showDatePicker(view, DATE_PICKER_END, parseDate(this.startDate), parseDate(this.endDate));
//        }
//    }

    @ReactProp(name = "date")
    public void setDate(DatePicker view, ReadableArray date) {
        if(date.size() != 3) return;
        showDatePicker(view, date.getInt(2), parseDate(date.getString(0)), parseDate(date.getString(1)));

    }

    @ReactProp(name = "returnDate")
    public void setReturnDate(DatePicker view, boolean returnDate) {
        if(!returnDate) return;
        int year = view.getYear();
        int month = view.getMonth() + 1;
        int day = view.getDayOfMonth();
        String dateString = year + "." + String.format("%02d", month) + "." + String.format("%02d", day);
        WritableMap params = Arguments.createMap();
        params.putString("date",dateString);
        sendEvent(reactContext, "ReturnDate", params);
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }
}
