package com.ford.cvs.caq.client.react_native;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.ford.cvs.caq.client.charts.CaqColumnChartView;

/**
 * Created by Administrator on 2017/3/3.
 */

public class CaqColumnChartViewManager extends SimpleViewManager<CaqColumnChartView> {

    private static final String REACT_CLASS = "RCTCaqColumnChartView";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected CaqColumnChartView createViewInstance(ThemedReactContext reactContext) {
        return new CaqColumnChartView(reactContext);
    }

    @ReactProp(name = "date")
    public void setDate(CaqColumnChartView view, ReadableArray date) {
        view.initData();
    }

}
