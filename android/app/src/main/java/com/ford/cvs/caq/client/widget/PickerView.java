package com.ford.cvs.caq.client.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;

import com.ford.cvs.caq.client.R;

/**
 * Created by Administrator on 2017/3/10.
 */

public class PickerView extends android.support.v7.widget.AppCompatTextView {

    private String key;

    public PickerView(Context context) {
        super(context);
        init();
    }

    public PickerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PickerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        Drawable rightDrawable = getResources().getDrawable(R.mipmap.arrow_bottom);
        rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());

        setTextSize(16);
        setTextColor(getResources().getColor(R.color.title_gray));
        setSingleLine();
        setCompoundDrawablePadding(5);
        setCompoundDrawables(null, null, rightDrawable, null);
        setGravity(Gravity.CENTER_VERTICAL);
        setText("全部");
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
