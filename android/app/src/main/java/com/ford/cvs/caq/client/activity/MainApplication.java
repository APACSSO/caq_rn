package com.ford.cvs.caq.client.activity;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.ford.cvs.caq.client.BuildConfig;
import com.ford.cvs.caq.client.react_native.CaqColumnChartViewReactPackage;
import com.ford.cvs.caq.client.react_native.DashboardViewReactPackage;
import com.ford.cvs.caq.client.react_native.DatePickerViewReactPackage;
import com.ford.cvs.caq.client.react_native.PickerViewReactPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
          new DashboardViewReactPackage(),
          new CaqColumnChartViewReactPackage(),
          new DatePickerViewReactPackage(),
          new PickerViewReactPackage()
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
