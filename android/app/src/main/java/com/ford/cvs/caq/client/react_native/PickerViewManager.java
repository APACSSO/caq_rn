package com.ford.cvs.caq.client.react_native;

import android.support.annotation.Nullable;
import android.view.View;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.ford.cvs.caq.client.widget.ActionItem;
import com.ford.cvs.caq.client.widget.PickerView;
import com.ford.cvs.caq.client.widget.QuickAction;

/**
 * Created by Administrator on 2017/3/10.
 */

public class PickerViewManager extends SimpleViewManager<PickerView> {

    private static final String REACT_CLASS = "RCTPickerView";

//    private static final String STATE_CHANGED_EVENT = "STATE_CHANGED_EVENT";

    private ThemedReactContext reactContext;

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected PickerView createViewInstance(ThemedReactContext reactContext) {
        this.reactContext = reactContext;
        final PickerView pickerView = new PickerView(reactContext);
//        final QuickAction quickAction = new QuickAction(reactContext, QuickAction.VERTICAL);
//
//        ActionItem statusItem1= new ActionItem(1, "全部", null);
//        ActionItem statusItem2 = new ActionItem(2, "已下载",null);
//        ActionItem statusItem3= new ActionItem(3, "未下载", null);
//        quickAction.addActionItem(statusItem1);
//        quickAction.addActionItem(statusItem2);
//        quickAction.addActionItem(statusItem3);
//
//        pickerView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                quickAction.show(v);
//            }
//        });
//
//        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
//            @Override
//            public void onItemClick(QuickAction source, int pos, int actionId) {
//                ActionItem actionItem = source.getActionItem(pos);
//                pickerView.setText(actionItem.getTitle());
//            }
//        });

        return pickerView;
    }

    @ReactProp(name = "pickerKey")
    public void setKey(final PickerView view, String key) {
        view.setKey(key);
    }

//    @Override
//    public Map<String, Object> getConstants() {
//        final Map<String, Object> constants = new HashMap<>();
//        constants.put(STATE_CHANGED_EVENT, STATE_CHANGED_EVENT);
//        return constants;
//    }

    @ReactProp(name = "item")
    public void setItem(final PickerView view, ReadableArray item) {
        final QuickAction quickAction = new QuickAction(view.getContext(), QuickAction.VERTICAL);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quickAction.show(v);
            }
        });
        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                ActionItem actionItem = source.getActionItem(pos);
                view.setText(actionItem.getTitle());
                WritableMap params = Arguments.createMap();
                params.putString("key",view.getKey());
                params.putInt("state", pos);
                sendEvent(reactContext, "STATE_CHANGED_EVENT", params);
            }
        });
        for (int i = 0; i < item.size(); i++) {
            ActionItem statusItem1 = new ActionItem(i, item.getString(i), null);
            quickAction.addActionItem(statusItem1);
        }
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }
}
