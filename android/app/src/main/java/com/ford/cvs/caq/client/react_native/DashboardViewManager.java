package com.ford.cvs.caq.client.react_native;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.ford.cvs.caq.client.widget.DashboardView;

/**
 * Created by Administrator on 2017/3/3.
 */

public class DashboardViewManager extends SimpleViewManager<DashboardView> {

    private static final String REACT_CLASS = "RCTDashboardView";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected DashboardView createViewInstance(ThemedReactContext reactContext) {
        return new DashboardView(reactContext);
    }

    @ReactProp(name = "percent")
    public void setPercent(DashboardView view, String percent) {
        view.setPercent(Integer.parseInt(percent));
    }
}
