import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  TextInput,
  Animated,
  TouchableOpacity,
  Easing,
  ListView,
  TouchableHighlight,
  Modal,
} from 'react-native';

import PmTipDialog from "./PmTipDialog.js"

var Dimensions = require('Dimensions');
var { width, height } = Dimensions.get('window');

class RealTimeScreen extends Component{

  constructor(props) {
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    var array = this._getDefaultData();
    this.state = {
      rotateValue: new Animated.Value(0),
      show: true,
      isSearching: false,
      isOk: false,
      dataSource: ds.cloneWithRows(array),
      showDeviceDetail:false,
      modalVisible:false
    };
  }

render(){
    return(
      <View style = {{backgroundColor:"green", flex:1}}>
        {this.state.showDeviceDetail &&
          this._deviceDetailRender()
        }
        {!this.state.showDeviceDetail &&
          this._searchingRender()
        }
      </View>
    );
  }

  _deviceDetailRender(){
    return (

      <View style = {styles.flex}>
        <View style = {styles.flex_t}>
          <View style = {styles.flex_tile}>
            <TouchableHighlight style = {styles.flex_tile1} onPress={() => this._goBack()}>
              <Image source={require('./img/back_icon.png')} style={styles.tabIcon_t} />
            </TouchableHighlight>
            <View style = {styles.flex_tile2}>
              <Text style = {styles.flex_tile_w}>APPLink</Text>
            </View>
            <View style = {{paddingRight:10, flexDirection:"row", alignItems:"center"}}>
                {this._pmTipRender()}
              <TouchableHighlight  underlayColor="#0000" onPress={() => {this.setState({modalVisible: true});}}>
                <Image source={require("./img/tips_icon.png")} style={styles.device_detail_top_icon} />
              </TouchableHighlight>
              <TouchableHighlight  underlayColor="#0000" onPress={() => {this.setState({showDeviceDetail:false});}}>
                <Image source={require("./img/search_device_icon.png")} style={styles.device_detail_top_icon} />
              </TouchableHighlight>
            </View>
          </View>
          <View style = {styles.flex_a_t} >
            <Text style = {styles.flex_a_t_a}>5°</Text>
            <View style = {styles.flex_a_t_b}>
            <Text style = {[styles.flex_a_t_c,{marginTop:23,color:"#ffffff",fontSize:13,}]}>湿度80%</Text>
            <Text style = {styles.flex_a_t_c}>北风2级别</Text>
            </View>
            <View style = {[styles.flex_a_t_b,{flex: 1,flexDirection:"row-reverse",}]}>

            <View style = {{marginRight:10,}}>
              <Text style = {[styles.flex_a_t_c,{marginTop:23,color:"#ffffff",fontSize:13,}]}>洗车指数 4 </Text>
              <Text style = {styles.flex_a_t_c}>事宜洗车</Text>
            </View>
            </View>
          </View>
          <View style = {styles.flex_a_c} >
            <View style = {styles.flex_bg_a} >
              <View style = {styles.flex_bg_b} >
              </View>
              <View style = {styles.flex_bg_e} >
                <View style = {styles.flex_bg_c} >
                  <Text style = {styles.flex_bg_cc}>车外</Text>
                </View>
              </View>
              <View style = {styles.flex_bg_f} >
                <Text style = {styles.flex_bg_cc_a}>109</Text>
              </View>
              <View style = {styles.flex_bg_h} >
                <Image source={require('./img/car.9.png')} style = {styles.flex_bg_h_img} />
              </View>
              <View style = {styles.flex_bg_che} >
                <View style = {styles.flex_bg_e} >
                  <View style = {styles.flex_bg_c} >
                    <Text style = {styles.flex_bg_cc}>车外</Text>
                  </View>
                </View>
                <View style = {styles.flex_bg_f} >
                  <Text style = {styles.flex_bg_cc_a}>46</Text>
                  <View style = {styles.flex_bg_x} >
                    <Image source={require('./img/tips_red_icon.png')} style = {styles.flex_bg_x_img} />
                  </View>
                </View>
                <View style = {styles.flex_bg_f} >
                  <Text style = {styles.flex_bg_cc}>建议更换空气滤清,打开空调外循环!</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }

  _pmTipRender(){
    return (
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {this.setState({modalVisible: false});}}
        >
        <TouchableHighlight style={{flex:1}} underlayColor="#0000" onPress={() => {this.setState({modalVisible: false});}}>
          <View style={{flex:1, backgroundColor:'rgba(0, 0, 0, 0.5)'}}>
            <View style={{flex:1, marginLeft:22, marginRight:22, marginTop:48, marginBottom:96}}>
              <TouchableHighlight style={{flex:1}}>
                <View style={{flex:1}} >
                  <PmTipDialog/>
                </View>
              </TouchableHighlight>
            </View>
          </View>
        </TouchableHighlight>
      </Modal>
    );
  }

  _searchingRender() {
    return (

      <View style = {styles.flex}>
        <View style = {styles.flex_t}>
          <View style = {styles.flex_tile}>
            <TouchableHighlight style = {styles.flex_tile1} onPress={() => this._goBack()}>
            <Image source={require("./img/back_icon.png")} style={styles.tabIcon_t}/>
            </TouchableHighlight>
            <View style = {styles.flex_tile2}>
              <Text style = {styles.flex_tile_w}>添加设备</Text>
            </View>
            <View style = {styles.flex_tile1}>
            </View>
          </View>

          <View style = {styles.flex_a_t} >
            <Text style = {styles.flex_a_t_a}>5°</Text>
            <View style = {styles.flex_a_t_b}>
            <Text style = {[styles.flex_a_t_c,{marginTop:23,color:"#ffffff",fontSize:13,}]}>湿度80%</Text>
            <Text style = {styles.flex_a_t_c}>北风2级别</Text>
            </View>
            <View style = {[styles.flex_a_t_b,{flex: 1,flexDirection:"row-reverse",}]}>

            <View style = {{marginRight:10,}}>
              <Text style = {[styles.flex_a_t_c,{marginTop:23,color:"#ffffff",fontSize:13,}]}>洗车指数 4 </Text>
              <Text style = {styles.flex_a_t_c}>事宜洗车</Text>
            </View>


            </View>
          </View>
          <View style = {[styles.flex_a_c]} >
              {this._searchImage()}
              {this._deviceList()}
          </View>


        </View>

      </View>
    );
  }

  _getDefaultData() {
     var deviceId = "设备1";
      var array = new Array;
      array[0] = deviceId;
      array[1] = deviceId;
      array[2] = deviceId;
      array[3] = deviceId;
      array[4] = deviceId;
      array[5] = deviceId;
      return array;
   }

    _renderRow(rowData, sectionID, rowID, highlightRow) {
      return (
        <TouchableHighlight underlayColor="rgb(210, 230, 255)"
          onPress={() => {
          this._clickJump(sectionID, rowID);
          highlightRow(sectionID, rowID);
        }}>
          <View style={{flex:1, flexDirection: 'row', alignItems:"center", justifyContent:"space-between", padding: 15}}>
            <Text style={{color:"#a9c8d8"}}>{rowData}</Text>
            <Image style={{width:15, height:15}} source={require("./img/arrow_right.png")}></Image>
          </View>
        </TouchableHighlight>
      );
    }

  _clickJump(sectionID, rowID){
    this.setState({showDeviceDetail:true});
  }

  _startAnimation() {
    this.setState((state) => ({isSearching: true}));
    this.state.rotateValue.setValue(0);
    Animated.parallel([
      Animated.timing(this.state.rotateValue, {
        fromValue: 0,
        toValue: 1,
        duration: 2000,
        easing: Easing.out(Easing.linear),
      }),
    ]).start(() => (this.setState((state) => ({isSearching: false, isOk: !state.isOk}))));
  }

  _searchImage() {
    return(
      <View style={{alignItems:"center", margin:20}}>
        <View style={[styles.search_image, {position:"relative", alignItems:"center"}]}>
          {!this.state.isSearching &&
          <Text style={{textAlign:"center", color:"#7ba7c9", fontSize:15}}>
            {this.state.isOk?"点击搜索" : "重新搜索"}
          </Text>}
          <Animated.Image
            source={require("./img/search_device_bg.png")}
            style={
              [styles.search_image, {transform:[
                {rotateZ: this.state.rotateValue.interpolate({
                  inputRange: [0,1],
                  outputRange: ['0deg', '360deg']})}]}]}/>
          <TouchableOpacity  style={styles.search_image} activeOpacity={1} onPress ={() => this._startAnimation()}>
            <Image source={require("./img/search_device_center_bg.png")} style={styles.search_image}/>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  _deviceList(){
    return(
      <View style={{flex:1}}>
        {!this.state.isOk &&
        <View style = {styles.flex_a_c_b} >
          <Text style = {styles.flex_a_c_c}>搜索失败</Text>
          <Text style = {styles.flex_a_c_d}>请确认设备蓝牙已经打开，并点击上方按键重新搜索。</Text>
        </View>}
        {this.state.isOk &&
        <View style={{flex:1}}>
          <Text style={{textAlign:"center", color:"#fff", backgroundColor:"#366791", fontSize:18, padding:5}}>搜索到设备</Text>
          <ListView
            dataSource={this.state.dataSource}
            renderRow={this._renderRow.bind(this)}
          />
        </View>}
     </View>
    );
  }

  _goBack() {
      const navigator = this.props.navigator;
      if(navigator){
        navigator.pop();
      }
      return true;
  }

}


const styles = StyleSheet.create({
search_image:{height:180, width:180, position:'absolute', top:0},
device_detail_top_icon: {width: 24,height: 24,margin:5},

flex_bg_x:{
	position: "absolute",
	left:(width*0.5)+70,
	top:25,
},
flex_bg_x_img:{
	width:24,
	height:24,
},
flex_bg_a:{
	flex:1,
},
flex_bg_che_kong:{

},
flex_bg_che:{
	flex:1,
	flexDirection: 'column',
	backgroundColor:"#8bc24a",
},
flex_bg_h_img:{
	width:width,
	height:width*0.18,
},
flex_bg_h:{
	marginTop:height*0.05,
},
flex_bg_cc:{
	lineHeight:21,
	textAlign:"center",
	color:"#ffffff",
	fontSize:14,
	opacity: 0.8,
},
flex_bg_cc_a:{
	textAlign:"center",
	color:"#ffffff",
	fontSize:55,

},
flex_bg_c:{
	width:50,
	height:25,
	borderWidth:1,
	borderTopLeftRadius: 11,
		borderTopRightRadius: 11,
		borderBottomLeftRadius: 11,
		borderBottomRightRadius: 11,
	borderBottomWidth: 2,
		borderColor: "#ffffff",
		opacity: 0.8,
},
flex_bg_e:{
	flexDirection:"row",
	justifyContent:"center",
		position: "relative",
},
flex_bg_b:{
	height:height*0.06,

},


flex_a_c_d:{
  color:"#8db9d5",
  fontSize:13,
},
flex_a_c_c:{
  color:"#ffffff",
  fontSize:17,
},
flex_a_c_b:{
  flex: 1,
  flexDirection:"column",
  justifyContent:"center",
  alignItems: 'center',
},
flex_a_c_a:{
  flex: 2,
  height:width/1.27,
  flexDirection:"row",
  justifyContent:"center",
  alignItems: 'center',
},
flex_a_c:{
  backgroundColor:"#2c7eb2",
  flex: 1,
},
flex_a_t_c:{
  color:"#ffffff",
  fontSize:13,
},
flex_a_t_b:{
  height:82,

},
flex_a_t_a:{

  fontSize:34,
  color:"#ffffff",
  height:82,
  marginTop:15,
  paddingLeft:15,
  paddingRight:15,
},
flex_a_t:{
  flexDirection: 'row',
  height:82,
  borderWidth:1,
  borderColor:"#2873a2",
  backgroundColor:"#2c7eb2",

},
flex_banner_cb:{
  color:"#989898",
  fontSize:14,
  lineHeight:25,

},
flex_banner_ca:{
  textAlign:"center",
  color:"#63abd9",
  fontSize:17,
  height:35,
},
flex_banner_c:{
  backgroundColor:"#ffffff",
  flex: 1,
  padding:22,

},
flex_banner_s:{
  height:width/1.38,
  backgroundColor:"#f00",
},
flex_tile_w:{
  height: 48,
  alignItems: 'center',
  color:"#ffffff",
  fontSize:19,
  lineHeight:38,
},
tabIcon_t: {
    width: 18,
    height: 18,
    resizeMode: 'stretch',
    margin:15
},
flex_tile2:{
  flexDirection: 'column',
  alignItems: 'center',
  height:48,
  flex: 1,
},
flex_tile1:{
  width:48,
  height:48,

},
flex_tile:{
  flexDirection: 'row',
  justifyContent:"flex-start",
  backgroundColor:"#2878aa",
  height:48,
},
flex_t:{
  flex: 1,
  flexDirection: 'column',
},
flex:{
  backgroundColor:"#000",
  flex: 1,
  flexDirection: 'column',

  },
flex1:{
  position:"relative",
  backgroundColor: '#ffffff',
  height: 50,
  borderTopWidth:1,
  borderColor:"#dadada",
  flexDirection: 'row',

  },
tabIcon: {
    width: 25,
    height: 50,
    resizeMode: 'stretch',
},
tab: {
  flex: 1,
  height: 50,
  backgroundColor: '#ffffff',
  alignItems: 'center',
},
tab_: {
  flex: 1,
  height: 50,
  backgroundColor: '#c6dcea',
  alignItems: 'center',
},
});

module.exports = RealTimeScreen