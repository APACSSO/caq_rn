import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  TextInput
} from 'react-native';

var Dimensions = require('Dimensions');
var { width, height } = Dimensions.get('window');

class PmTipDialog extends Component {

  render() {
    return (
      <View style={styles.flex_c_all}>
        <View style={styles.flex_c_a}>
          <View style={styles.flex_c_aa}>
            <Text style={styles.flex_c_b}>PM 2.5</Text>
          </View>
        </View>

        <View style={styles.flex_c_c}>
          <Text style={styles.flex_c_d}>中国环境环保局标准</Text>
        </View>

        <View style={styles.flex_c_e_a}>
          <View style={styles.flex_c_e_e}>
            <View style={styles.flex_c_e_b}>
              <Text style={styles.flex_c_e_f}>0-35</Text>
            </View>
          </View>
        </View>

        <View style={styles.flex_c_c}>
          <Text style={styles.flex_c_d}>空气质量良好</Text>
        </View>

        <View style={styles.flex_c_f_a}>
          <View style={styles.flex_c_f_e}>
            <View style={styles.flex_c_f_b_a}>
              <Text style={styles.flex_c_e_c}>36-75</Text>
            </View>
            <View style={styles.flex_c_f_d}>
              <Text style={styles.flex_c_f_c}>空气质量正常</Text>
            </View>
          </View>

          <View style={styles.flex_c_f_e}>
            <View style={styles.flex_c_f_b_b}>
              <Text style={styles.flex_c_e_c}>76-115</Text>
            </View>
            <View style={styles.flex_c_f_d}>
              <Text style={styles.flex_c_f_c}>轻度污染</Text>
            </View>
          </View>
          <View style={styles.flex_c_f_e}>
            <View style={styles.flex_c_f_b_c}>
              <Text style={styles.flex_c_e_c}>116-150</Text>
            </View>
            <View style={styles.flex_c_f_d}>
              <Text style={styles.flex_c_f_c}>中度污染</Text>
            </View>
          </View>
        </View>
        <View style={styles.flex_c_f_a}>
          <View style={styles.flex_c_f_e}>
            <View style={styles.flex_c_f_b_d}>
              <Text style={styles.flex_c_e_c}>151-250</Text>
            </View>
            <View style={styles.flex_c_f_d}>
              <Text style={styles.flex_c_f_c}>重度污染</Text>
            </View>
          </View>
          <View style={styles.flex_c_f_e}>
            <View style={styles.flex_c_f_b_e}>
              <Text style={styles.flex_c_e_c}>251-350</Text>
            </View>
            <View style={styles.flex_c_f_d}>
              <Text style={styles.flex_c_f_c}>严重污染</Text>
            </View>
          </View>
          <View style={styles.flex_c_f_e}>
            <View style={styles.flex_c_f_b_f}>
              <Text style={styles.flex_c_e_c}>351以上</Text>
            </View>
            <View style={styles.flex_c_f_d}>
              <Text style={styles.flex_c_f_c}>不宜人类生存</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  flex_c_f_b_a: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#8bc24a",
  },
  flex_c_f_b_b: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#dfc42d",
  },
  flex_c_f_b_c: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#fb8c00",
  },
  flex_c_f_b_d: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#e53937",
  },
  flex_c_f_b_e: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#614f83",
  },
  flex_c_f_b_f: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#3c3c34",
  },
  flex_c_f_a: {
    flexDirection: 'row',
    justifyContent: 'space-around',

  },
  flex_c_f_c: {
    fontSize: 14,
    color: "#939393",
    textAlign: "center",
    lineHeight: 28,
  },
  flex_c_f_e: {
    width: 96,
    marginTop: 10,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  flex_c_e_e: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  flex_c_e_c: {
    fontSize: 14,
    color: "#ffffff",
    textAlign: "center",
    lineHeight: 45,
  },
  flex_c_e_f: {
    fontSize: 17,
    color: "#ffffff",
    textAlign: "center",
    lineHeight: 58,
  },
  flex_c_e_b: {
    width: 94,
    height: 94,
    borderTopLeftRadius: 47,
    borderTopRightRadius: 47,
    borderBottomLeftRadius: 47,
    borderBottomRightRadius: 47,
    backgroundColor: "#8ed0bb",

  },
  flex_c_all: {
    backgroundColor: "#ffffff",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    paddingBottom: 20,
  },
  flex_c_d: {
    color: "#b0b0b0",
    fontSize: 12,
    textAlign: "center",
    marginTop: 5,
    marginLeft: 10,
  },
  flex_c_c: {

  },
  flex_c_aa: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',

  },
  flex_c_b: {
    color: "#ffffff",
    backgroundColor: "#37bcb0",
    textAlign: "center",
    height: 40,
    fontSize: 25,
    lineHeight: 35,
    width: 180,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  flex_c_a: {

    height: 40,

  },
  flex_c_cc: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  flex_b_e_img: {
    marginTop: 18,
    marginLeft: 25,
  },
  flex_b_cc: {
    flex: 1,
    flexDirection: "row",
  },
  flex_b_d: {
    flex: 1,
    color: "#ffffff",
    fontSize: 16,
    height: 52,
    lineHeight: 37,
    textAlign: "left",
    width: 100,
    paddingLeft: 15,
  },
  flex_b_e: {
    width: 50,
    height: 52,
  },
  flex_b_c: {
    borderBottomWidth: 1,
    borderColor: "#23658e",

    backgroundColor: "#276f9c",
    height: 52,

  },
  flex_b_a: {
    backgroundColor: "#266791",
    height: 42,

  },
  flex_b_b: {
    flex: 1,
    color: "#ffffff",
    fontSize: 18,
    height: 42,
    textAlign: "center",
    lineHeight: 32,
  },
  flex_a_c_d: {
    color: "#8db9d5",
    fontSize: 13,
  },
  flex_a_c_c: {
    color: "#ffffff",
    fontSize: 17,
  },
  flex_b_c_b: {
    flex: 1,
    flexDirection: "column",
  },
  flex_a_c_b: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: 'center',
  },
  flex_a_c_a: {
    paddingTop: 10,
    paddingBottom: 10,
    flex: 1,
    height: width / 1.27,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: 'center',
  },
  flex_a_c: {
    backgroundColor: "#2c7eb2",
    flex: 1,

  },
  flex_a_t_c: {
    color: "#ffffff",
    fontSize: 13,
  },
  flex_a_t_b: {
    height: 82,

  },
  flex_a_t_a: {

    fontSize: 34,
    color: "#ffffff",
    height: 82,
    marginTop: 15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  flex_a_t: {
    flexDirection: 'row',
    height: 82,
    borderWidth: 1,
    borderColor: "#2873a2",
    backgroundColor: "#2c7eb2",

  },
  flex_banner_cb: {
    color: "#989898",
    fontSize: 14,
    lineHeight: 25,

  },
  flex_banner_ca: {
    textAlign: "center",
    color: "#63abd9",
    fontSize: 17,
    height: 35,
  },
  flex_banner_c: {
    backgroundColor: "#ffffff",
    flex: 1,
    padding: 22,

  },
  flex_banner_s: {
    height: width / 1.38,
    backgroundColor: "#f00",
  },
  flex_tile_w: {
    height: 48,
    alignItems: 'center',
    color: "#ffffff",
    fontSize: 19,
    lineHeight: 38,
  },
  tabIcon_t: {
    width: 48,
    height: 48,
    resizeMode: 'stretch',
  },
  flex_tile2: {
    flexDirection: 'column',
    alignItems: 'center',
    height: 48,
    flex: 1,
  },
  flex_tile1: {
    width: 48,
    height: 48,

  },
  flex_tile: {
    flexDirection: 'row',
    justifyContent: "flex-start",
    backgroundColor: "#2878aa",
    height: 48,
    borderTopLeftRadius: 17,
    borderTopRightRadius: 17,
  },
  flex_t: {
    flex: 1,
    flexDirection: 'column',
  },
  flex: {
    backgroundColor: "#000",
    flex: 1,
    flexDirection: 'column',

  },
  flex1: {
    position: "relative",
    backgroundColor: '#ffffff',
    height: 50,
    borderTopWidth: 1,
    borderColor: "#dadada",
    flexDirection: 'row',

  },
  tabIcon: {
    width: 25,
    height: 50,
    resizeMode: 'stretch',
  },
  tab: {
    flex: 1,
    height: 50,
    backgroundColor: '#ffffff',
    alignItems: 'center',
  },
  tab_: {
    flex: 1,
    height: 50,
    backgroundColor: '#c6dcea',
    alignItems: 'center',
  },
});

module.exports = PmTipDialog;
