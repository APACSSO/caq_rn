'use strict'

var ProgressBar = require('ProgressBarAndroid');
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  View,
  TouchableHighlight,
  ListView,
  Text, Platform, Image, Navigator, Picker, requireNativeComponent,
  DeviceEventEmitter,
} from 'react-native';

import MainRoute from './MainRoute.js'
var PickerView = requireNativeComponent('RCTPickerView', PickerView);

function SmallAppInfo(img, name, isAppLink, comment, progress, statue) {
  var smallAppInfo = new Object;
  smallAppInfo.img = img;
  smallAppInfo.name = name;
  smallAppInfo.isAppLink = isAppLink;
  smallAppInfo.comment = comment;
  smallAppInfo.progress = progress;
  smallAppInfo.statue = statue;
  return smallAppInfo;
}

const L_XX = require('./img/cancel_icon.png');
var Dimensions = require('Dimensions');
var { width, height } = Dimensions.get('window');

var STATUS_KEY = "status_key";
var CATEGORY_KEY = "category_key";

class CaqProject extends Component {
  render() {
    return (
      <Navigator
        initialRoute={{ component: AppSelection }}
        configureScene={
          (route) => {
            return Navigator.SceneConfigs.FloatFromRight;
          }
        }
        renderScene={
          (route, navigator) => {
            let Component = route.component;
            return <Component{...route.params} navigator={navigator} />
          }
        }
      />
    );
  }
}

class AppSelection extends Component {

  constructor(props) {
    super(props)
    var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => { return true } });
    this._data = [];
    this._getDefaultData();
    this.state = {
      dataSource: ds.cloneWithRows(this._data),
      statusSelected: 0,
      categorySelected: 0,
    };
  }

  componentWillMount() {
    DeviceEventEmitter.addListener("STATE_CHANGED_EVENT", (msg) => {
      if (msg.key === STATUS_KEY) {
        this.setState({ statusSelected: msg.state });
        this.setState({ dataSource: this.state.dataSource.cloneWithRows(this._data) });
      } else if (msg.key === CATEGORY_KEY) {
        this.setState({ categorySelected: msg.state });
        this.setState({ dataSource: this.state.dataSource.cloneWithRows(this._data) });
      }
    });
  }

  render() {
    return (
      <View style={styles.flex_t}>
        <View style={styles.flex_tile}>
          <View style={styles.flex_tile1}>
            <Image source={L_XX} style={styles.tabIcon_t} />
          </View>
          <View style={styles.flex_tile2}>
            <Text style={styles.flex_tile_w}>福特小程序</Text>
          </View>
          <View style={styles.flex_tile1}>
          </View>
        </View>

        <View style={styles.flex_f_c}>
          <View style={styles.flex_f_c_a}>
            <View style={styles.flex_f_c_b}>
              <Text style={styles.flex_f_c_d}>状态:</Text>
            </View>
            <View style={{ justifyContent: "center", flex: 1 }}>
              <PickerView style={{ flex: 1, margin: 10 }} pickerKey={STATUS_KEY} item={["全部", "已下载", "未下载"]} />
            </View>
          </View>
          <View style={styles.flex_f_c_a}>
            <View style={styles.flex_f_c_b}>
              <Text style={styles.flex_f_c_d}>类别:</Text>
            </View>
            <View style={{ justifyContent: "center", flex: 1 }}>
              <PickerView style={{ flex: 1, margin: 10 }} pickerKey={CATEGORY_KEY} item={["全部", "AppLink应用", "非AppLink应用"]} />
            </View>
          </View>
        </View>

        <ListView
          dataSource={this.state.dataSource}
          renderRow={this._renderRow.bind(this)}
        />
      </View>
    );
  }

  _onValueChange = (key: string, value: string) => {
    const newState = {};
    newState[key] = value;
    this.setState(newState);
  };

  _clickJump(rowData, rowID) {
    if (rowData.progress < 100) {
      !this._data[rowID].timer && (this._data[rowID].timer = setInterval(
        () => {
          var tmp = this._data[rowID].progress += 2;
          (tmp >= 100) && this._data[rowID].timer && clearInterval(this._data[rowID].timer);
          this.setState({ dataSource: this.state.dataSource.cloneWithRows(this._data) });
        },
        50
      ));
    } else {
      const navigator = this.props.navigator;
      if (navigator) {
        navigator.push({
          component: MainRoute
        });
      }
    }
  }

  _getDefaultData() {
    var smallAppInfo1 = SmallAppInfo(
      require("./img/small_app_icon.png"),
      "福特应用汇",
      false,
      "及时使用最新福特AppLink应用.",
      100);
    var smallAppInfo2 = SmallAppInfo(
      require("./img/small_app_icon.png"),
      "福特应用汇",
      true,
      "及时使用最新福特AppLink应用.",
      0);
    var smallAppInfo3 = SmallAppInfo(
      require("./img/small_app_icon.png"),
      "福特应用汇",
      false,
      "及时使用最新福特AppLink应用.",
      0);
    var smallAppInfo4 = SmallAppInfo(
      require("./img/small_app_icon.png"),
      "福特应用汇",
      true,
      "及时使用最新福特AppLink应用.",
      100);
    this._data[0] = smallAppInfo1;
    this._data[1] = smallAppInfo2;
    this._data[2] = smallAppInfo3;
    this._data[3] = smallAppInfo4;
  }

  _renderRow(rowData, sectionID, rowID, highlightRow) {
    if ((rowData.progress == 100) && this.state.statusSelected == 2) {
      return (null);
    }
    if (rowData.isAppLink && this.state.categorySelected == 2) {
      return (null);
    }
    if ((rowData.progress < 100) && this.state.statusSelected == 1) {
      return (null);
    }
    if (!rowData.isAppLink && this.state.categorySelected == 1) {
      return (null);
    }
    return (
      <View>
        <View style={styles.flex_f_l_a}>
          <View style={styles.flex_f_l_l}>
            <View style={styles.flex_f_l_l_l}>
              <Image source={rowData.img} />
            </View>
            <View style={styles.flex_f_l_l_r}>
              <Text style={{ color: "#2a2727", fontSize: 16 }}>{rowData.name}</Text>
              <Text style={{ color: "#9a9393", fontSize: 13 }}>{rowData.isAppLink ? "AppLink应用" : "非AppLink应用"}</Text>
              <Text style={{ color: "#363232", fontSize: 13 }}>{rowData.comment}</Text>
            </View>
          </View>
          <TouchableHighlight underlayColor="rgb(210, 230, 255)"
            onPress={() => {
              this._clickJump(rowData, rowID);
              highlightRow(sectionID, rowID);
            }}>
            <View style={styles.flex_f_l_r}>
              <Image style={{ width: 32, height: 32 }} source={rowData.progress == 100 ? require("./img/open_icon.png") : require("./img/download_icon.png")} />
              <Text>{rowData.progress == 100 ? "打开" : "下载"}</Text>
            </View>
          </TouchableHighlight>
        </View>
        <ProgressBar styleAttr="Horizontal" progress={rowData.progress / 100} indeterminate={false} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flex_f_c_d: {
    color: "#010101",
    fontSize: 17,
    lineHeight: 37,
    paddingLeft: 10,
  },
  flex_f_c_a: {
    flex: 1,
    flexDirection: "row",
  },
  flex_f_c_b: {
    flex: 1,

  },
  flex_f_x: {
    height: 2,
    backgroundColor: "#439def",
    width: width,
    marginTop: -2,
  },
  xz_img_wz_a: {
    color: "#2a2727",
    fontSize: 17,
  },
  xz_img_wz_b: {
    color: "#9a9393",
    fontSize: 13,
  },
  xz_img_wz_c: {
    color: "#363232",
    fontSize: 13,
  },
  flex_f_l_l_r: {
    flex: 1,
    justifyContent: "center",
  },
  flex_f_l_l_l: {
    width: 103,
    justifyContent: "center",
    alignItems: 'center',
  },
  xz_img_wz: {
    color: "#9a9393",
    fontSize: 13,
    marginTop: 7,
  },
  xz_img: {

  },
  flex_f_l_r: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: 'center',
    width: 74,
    height: 55,
    borderLeftColor: "#ccc7c7",
    borderLeftWidth: 1,
  },
  flex_f_l_l: {
    flex: 1,
    height: 103,
    flexDirection: "row",
  },
  flex_f_l_a: {
    flexDirection: "row",
    alignItems: 'center',
    height: 103,
  },
  flex_f_list: {
    backgroundColor: "#ebebeb",
    flex: 1,
  },
  flex_f_c: {
    height: 52,
    borderBottomWidth: 2,
    borderColor: "#ebebeb",
    backgroundColor: "#fff",
    flexDirection: "row",
  },













  flex_c_f_b_a: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#8bc24a",
  },
  flex_c_f_b_b: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#dfc42d",
  },
  flex_c_f_b_c: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#fb8c00",
  },
  flex_c_f_b_d: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#e53937",
  },
  flex_c_f_b_e: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#614f83",
  },
  flex_c_f_b_f: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#3c3c34",
  },
  flex_c_f_a: {
    flexDirection: 'row',
    justifyContent: 'space-around',

  },
  flex_c_f_c: {
    fontSize: 14,
    color: "#939393",
    textAlign: "center",
    lineHeight: 28,
  },
  flex_c_f_e: {
    width: 96,
    marginTop: 10,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  flex_c_e_e: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  flex_c_e_c: {
    fontSize: 14,
    color: "#ffffff",
    textAlign: "center",
    lineHeight: 45,
  },
  flex_c_e_f: {
    fontSize: 17,
    color: "#ffffff",
    textAlign: "center",
    lineHeight: 58,
  },
  flex_c_e_b: {
    width: 94,
    height: 94,
    borderTopLeftRadius: 47,
    borderTopRightRadius: 47,
    borderBottomLeftRadius: 47,
    borderBottomRightRadius: 47,
    backgroundColor: "#8ed0bb",

  },
  flex_c_all: {
    backgroundColor: "#ffffff",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    paddingBottom: 20,
  },
  flex_c_d: {
    color: "#b0b0b0",
    fontSize: 12,
    textAlign: "center",
    marginTop: 5,
    marginLeft: 10,
  },
  flex_c_c: {

  },
  flex_c_aa: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',

  },
  flex_c_b: {
    color: "#ffffff",
    backgroundColor: "#37bcb0",
    textAlign: "center",
    height: 40,
    fontSize: 25,
    lineHeight: 35,
    width: 180,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  flex_c_a: {

    height: 40,

  },
  flex_c_cc: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  flex_b_e_img: {
    marginTop: 18,
    marginLeft: 25,
  },
  flex_b_cc: {
    flex: 1,
    flexDirection: "row",
  },
  flex_b_d: {
    flex: 1,
    color: "#ffffff",
    fontSize: 16,
    height: 52,
    lineHeight: 37,
    textAlign: "left",
    width: 100,
    paddingLeft: 15,
  },
  flex_b_e: {
    width: 50,
    height: 52,
  },
  flex_b_c: {
    borderBottomWidth: 1,
    borderColor: "#23658e",

    backgroundColor: "#276f9c",
    height: 52,

  },
  flex_b_a: {
    backgroundColor: "#266791",
    height: 42,

  },
  flex_b_b: {
    flex: 1,
    color: "#ffffff",
    fontSize: 18,
    height: 42,
    textAlign: "center",
    lineHeight: 32,
  },
  flex_a_c_d: {
    color: "#8db9d5",
    fontSize: 13,
  },
  flex_a_c_c: {
    color: "#ffffff",
    fontSize: 17,
  },
  flex_b_c_b: {
    flex: 1,
    flexDirection: "column",
  },
  flex_a_c_b: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: 'center',
  },
  flex_a_c_a: {
    paddingTop: 10,
    paddingBottom: 10,
    flex: 1,
    height: width / 1.27,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: 'center',
  },
  flex_a_c: {
    backgroundColor: "#2c7eb2",
    flex: 1,

  },
  flex_a_t_c: {
    color: "#ffffff",
    fontSize: 13,
  },
  flex_a_t_b: {
    height: 82,

  },
  flex_a_t_a: {

    fontSize: 34,
    color: "#ffffff",
    height: 82,
    marginTop: 15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  flex_a_t: {
    flexDirection: 'row',
    height: 82,
    borderWidth: 1,
    borderColor: "#2873a2",
    backgroundColor: "#2c7eb2",

  },
  flex_banner_cb: {
    color: "#989898",
    fontSize: 14,
    lineHeight: 25,

  },
  flex_banner_ca: {
    textAlign: "center",
    color: "#63abd9",
    fontSize: 17,
    height: 35,
  },
  flex_banner_c: {
    backgroundColor: "#ffffff",
    flex: 1,
    padding: 22,

  },
  flex_banner_s: {
    height: width / 1.38,
    backgroundColor: "#f00",
  },
  flex_tile_w: {
    height: 48,
    alignItems: 'center',
    color: "#ffffff",
    fontSize: 19,
    lineHeight: 38,
  },
  tabIcon_t: {
    width: 18,
    height: 18,
    resizeMode: 'stretch',
    margin: 15
  },
  flex_tile2: {
    flexDirection: 'column',
    alignItems: 'center',
    height: 48,
    flex: 1,
  },
  flex_tile1: {
    width: 48,
    height: 48,

  },
  flex_tile: {
    flexDirection: 'row',
    justifyContent: "flex-start",
    backgroundColor: "#2878aa",
    height: 48,
  },
  flex_t: {
    flex: 1,
    flexDirection: 'column',
  },
  flex: {
    backgroundColor: "#000",
    flex: 1,
    flexDirection: 'column',

  },
  flex1: {
    position: "relative",
    backgroundColor: '#ffffff',
    height: 50,
    borderTopWidth: 1,
    borderColor: "#dadada",
    flexDirection: 'row',

  },
  tabIcon: {
    width: 25,
    height: 50,
    resizeMode: 'stretch',
  },
  tab: {
    flex: 1,
    height: 50,
    backgroundColor: '#ffffff',
    alignItems: 'center',
  },
  tab_: {
    flex: 1,
    height: 50,
    backgroundColor: '#c6dcea',
    alignItems: 'center',
  },
});

module.exports = CaqProject