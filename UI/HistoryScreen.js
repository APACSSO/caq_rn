import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  TextInput,
  requireNativeComponent,
  TouchableHighlight,
  Modal,
  DatePickerAndroid,
  TouchableNativeFeedback,
  DeviceEventEmitter,
  ToastAndroid,
} from 'react-native';

var CaqColumnChartView = requireNativeComponent('RCTCaqColumnChartView', CaqColumnChartView);
var DatePickerView = requireNativeComponent('RCTDatePickerView', DatePickerView);

var Dimensions = require('Dimensions');
var { width, height } = Dimensions.get('window');
const COLOR = ['#8898ae', '#83d0c9']

class HistoryScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected:1,
      startDate:"2016.12.25",
      endDate:"2017.02.01",
      modalVisible:false,
      datePickerState: 0,
      returnDate: false,
    };
  }

  componentWillMount() {
    DeviceEventEmitter.addListener('ReturnDate', (msg) => {
        if(this.state.datePickerState == 1){
          this.setState({startDate:msg.date});
        }else if(this.state.datePickerState == 2){
           this.setState({endDate:msg.date});
        }
        this.setState({datePickerState: 0, returnDate:false});
      });
    }

  render() {
    return (

      <View style={styles.flex}>
              {this._datePickerRender()}
        <View style={styles.flex_t}>
          <View style={styles.flex_tile}>
            <TouchableHighlight style={styles.flex_tile1} onPress={() => this._goBack()}>
              <Image source={require("./img/back_icon.png")} style={styles.tabIcon_t} />
            </TouchableHighlight>
            <View style={styles.flex_tile2}>
              <Text style={styles.flex_tile_w}>历史数据统计</Text>
            </View>
            <TouchableNativeFeedback style={styles.flex_tile3} onPress={() => {this.setState({modalVisible: true});}}>
              <Text style={styles.flex_tile_w_r}>高级筛选</Text>
            </TouchableNativeFeedback>
          </View>

          <View style={{flex:1, height:48, flexDirection:"row", margin:24}}>
            <TouchableHighlight style={{flex:1, justifyContent:"center", borderTopLeftRadius:10, borderBottomLeftRadius:10, backgroundColor:this.state.selected == 0?COLOR[0]:COLOR[1]}}
              underlayColor={this.state.selected == 0?COLOR[0]:COLOR[1]}
              onPress={() => {
                this._updateSelectedDate("2017.01.01", "2017.01.01");
                this.setState({selected: 0});}}>
              <Text style={styles.date_select}>日</Text>
            </TouchableHighlight>
            <TouchableHighlight style={{flex:1, justifyContent:"center", backgroundColor:this.state.selected == 1?COLOR[0]:COLOR[1]}}
              underlayColor={this.state.selected == 0?COLOR[0]:COLOR[1]}
              onPress={() => {
                this._updateSelectedDate("2017.01.01", "2017.01.07");
                this.setState({selected: 1});}}>
              <Text style={styles.date_select}>周</Text>
            </TouchableHighlight>
            <TouchableHighlight style={{flex:1, justifyContent:"center", borderTopRightRadius:10, borderBottomRightRadius:10, backgroundColor:this.state.selected == 2?COLOR[0]:COLOR[1]}}
              underlayColor={this.state.selected == 0?COLOR[0]:COLOR[1]}
              onPress={() => {
                this._updateSelectedDate("2017.01.01", "2017.02.01");
                this.setState({selected: 2});}}>
              <Text style={styles.date_select}>月</Text>
            </TouchableHighlight>
          </View>

          <View style={[styles.flex_g_b_a, {margin:24}]}>
            <Text style={styles.flex_g_b_b}>车内PM2.5趋势图</Text>
          </View>

          <View style={{ alignItems: "center" }}>
            <CaqColumnChartView style={{ width: 320, height: 320 }} date={[this.state.startDate, this.state.endDate]}/>
          </View>
        </View>

      </View>
    );
  }

  _updateSelectedDate(startDate, endDate){
    this.setState({startDate:startDate, endDate:endDate});
  }

  _goBack() {
      const navigator = this.props.navigator;
      if(navigator){
        navigator.pop();
      }
      return true;
  }

  _datePickerRender() {
    return (
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {this.setState({modalVisible: false});}}
        >
        <TouchableHighlight style={{flex:1}} underlayColor="#0000" onPress={() => {this.setState({modalVisible: false, datePickerState:0});}}>
          <View style={{flex:1, backgroundColor:'rgba(0, 0, 0, 0.5)'}}>
            <View style={{flex:1, marginLeft:22, marginRight:22, marginTop:48}}>
              <TouchableHighlight style={{}}>
                <View style={{ backgroundColor:"white", borderRadius:15}} >
                  <View>
                    <View style={{flexDirection:"row", justifyContent:"space-between", padding:18}}>
                      <Text>起始时间</Text>
                      <TouchableNativeFeedback onPress={() => this.setState({datePickerState:1})}>
                        <Text style={{color:"#5d87c1"}}>{this.state.startDate}</Text>
                      </TouchableNativeFeedback>
                    </View>
                    <View style={{backgroundColor:"gray", height:1}}></View>
                    <View style={{flexDirection:"row", justifyContent:"space-between", padding:18}}>
                      <Text>结束时间</Text>
                      <TouchableNativeFeedback onPress={() => this.setState({datePickerState:2})}>
                        <Text style={{color:"#5d87c1"}}>{this.state.endDate}</Text>
                      </TouchableNativeFeedback>
                    </View>
                  </View>
                  {(this.state.datePickerState != 0) &&
                  <View>
                    <View style={{height:48, flexDirection:"row", justifyContent:"flex-start", backgroundColor:"#efefef"}}>
                      <View style={{flexDirection: 'column',justifyContent:"center", flex: 1}}>
                        <Text style={{fontSize:18, textAlign:"center"}}>
                          {this.state.datePickerState == 1 && "选择起始时间"}
                          {this.state.datePickerState == 2 && "选择结束时间"}
                        </Text>
                      </View>
                      <TouchableNativeFeedback onPress={() => {this.setState({returnDate:true});}}>
                        <Text style={{color:"#1b84ac", alignSelf:"center", padding:10}}>确定</Text>
                      </TouchableNativeFeedback>
                    </View>
                    <View style={{alignSelf:"center", alignItems:"center"}}>
                      <DatePickerView style={{ width: 240, height: 160}}
                        date={[this.state.startDate, this.state.endDate, this.state.datePickerState]}
                        returnDate={this.state.returnDate}/>
                    </View>
                  </View>}
                </View>
              </TouchableHighlight>
            </View>
          </View>
        </TouchableHighlight>
      </Modal>
    );
  }

}




const styles = StyleSheet.create({
  date_select:{fontSize:18, color:"white", textAlign:"center"},

  flex_g_b_l: {
    flex: 1,
    textAlign: "left",
    color: "#919191",
    fontSize: 17,
    paddingLeft: 20,
    lineHeight: 40,
  },
  flex_g_b_r: {
    paddingRight: 20,
    flex: 1,
    textAlign: "right",
    color: "#5d87c1",
    fontSize: 17,
    lineHeight: 40,
  },
  flex_g_ta_b: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: "#dbdbdb",
    height: 60,
    flexDirection: "row",
  },
  flex_g_ta_c: {
    height: 60,
    flex: 1,
    flexDirection: "row",
  },
  flex_g_ta_a: {
    flexDirection: "column",
    flex: 1,
    backgroundColor: "#fff",
    height: 125,
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
    padding: 2,
  },
  flex_g_ta: {
    height: 125,
    flexDirection: "row",
    justifyContent: "center",
    padding: 10,
  },
  flex_g_b_b: {
    color: "#63abd9",
    fontSize: 17,
    textAlign: "center",
    height: 20,
  },
  flex_g_b: {
    height: 58,
  },
  flex_g_t_e: {
    color: "#ffffff",
    fontSize: 17,
    textAlign: "center",
    lineHeight: 30,
  },
  flex_g_t_b: {

    width: width * 0.25,
    backgroundColor: "#83d0c9",
    height: 37,
  },
  flex_g_t_c: {
    width: width * 0.25,
    backgroundColor: "#8898ae",
    height: 37,
  },
  flex_g_t_a: {
    height: 37,
    flexDirection: "row",
    justifyContent: "center",
  },
  flex_g_a: {
    height: 20,
  },
  flex_tile3: {
    width: 70,
    height: 48,

  },
  flex_tile_w_r: {
    color: "#ffffff",
    fontSize: 15,
    lineHeight: 34,
    paddingRight: 10,
  },












  flex_f_c_d: {
    color: "#010101",
    fontSize: 17,
    lineHeight: 37,
    paddingLeft: 10,
  },
  flex_f_c_a: {
    flex: 1,
    flexDirection: "row",
  },
  flex_f_c_b: {
    flex: 1,

  },
  flex_f_x: {
    height: 2,
    backgroundColor: "#439def",
    width: width,
    marginTop: -2,
  },
  xz_img_wz_a: {
    color: "#2a2727",
    fontSize: 17,
  },
  xz_img_wz_b: {
    color: "#9a9393",
    fontSize: 13,
  },
  xz_img_wz_c: {
    color: "#363232",
    fontSize: 13,
  },
  flex_f_l_l_r: {
    flex: 1,
    justifyContent: "center",
  },
  flex_f_l_l_l: {
    width: 103,
    justifyContent: "center",
    alignItems: 'center',
  },
  xz_img_wz: {
    color: "#9a9393",
    fontSize: 13,
    marginTop: 7,
  },
  xz_img: {

  },
  flex_f_l_r: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: 'center',
    width: 74,
    height: 55,
    borderLeftColor: "#ccc7c7",
    borderLeftWidth: 1,
  },
  flex_f_l_l: {
    flex: 1,
    height: 103,
    flexDirection: "row",
  },
  flex_f_l_a: {
    flexDirection: "row",
    alignItems: 'center',
    height: 103,
    borderBottomWidth: 2,
    borderColor: "#ccc7c7",
    backgroundColor: "#fff",
  },
  flex_f_list: {
    backgroundColor: "#ebebeb",
    flex: 1,
  },
  flex_f_c: {
    height: 52,
    borderBottomWidth: 2,
    borderColor: "#ebebeb",
    backgroundColor: "#fff",
    flexDirection: "row",
  },













  flex_c_f_b_a: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#8bc24a",
  },
  flex_c_f_b_b: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#dfc42d",
  },
  flex_c_f_b_c: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#fb8c00",
  },
  flex_c_f_b_d: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#e53937",
  },
  flex_c_f_b_e: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#614f83",
  },
  flex_c_f_b_f: {
    marginLeft: 12,
    width: 72,
    height: 72,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    backgroundColor: "#3c3c34",
  },
  flex_c_f_a: {
    flexDirection: 'row',
    justifyContent: 'space-around',

  },
  flex_c_f_c: {
    fontSize: 14,
    color: "#939393",
    textAlign: "center",
    lineHeight: 28,
  },
  flex_c_f_e: {
    width: 96,
    marginTop: 10,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  flex_c_e_e: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  flex_c_e_c: {
    fontSize: 14,
    color: "#ffffff",
    textAlign: "center",
    lineHeight: 45,
  },
  flex_c_e_f: {
    fontSize: 17,
    color: "#ffffff",
    textAlign: "center",
    lineHeight: 58,
  },
  flex_c_e_b: {
    width: 94,
    height: 94,
    borderTopLeftRadius: 47,
    borderTopRightRadius: 47,
    borderBottomLeftRadius: 47,
    borderBottomRightRadius: 47,
    backgroundColor: "#8ed0bb",

  },
  flex_c_all: {
    backgroundColor: "#ffffff",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    paddingBottom: 20,
  },
  flex_c_d: {
    color: "#b0b0b0",
    fontSize: 12,
    textAlign: "center",
    marginTop: 5,
    marginLeft: 10,
  },
  flex_c_c: {

  },
  flex_c_aa: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',

  },
  flex_c_b: {
    color: "#ffffff",
    backgroundColor: "#37bcb0",
    textAlign: "center",
    height: 40,
    fontSize: 25,
    lineHeight: 35,
    width: 180,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  flex_c_a: {

    height: 40,

  },
  flex_c_cc: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  flex_b_e_img: {
    marginTop: 18,
    marginLeft: 25,
  },
  flex_b_cc: {
    flex: 1,
    flexDirection: "row",
  },
  flex_b_d: {
    flex: 1,
    color: "#ffffff",
    fontSize: 16,
    height: 52,
    lineHeight: 37,
    textAlign: "left",
    width: 100,
    paddingLeft: 15,
  },
  flex_b_e: {
    width: 50,
    height: 52,
  },
  flex_b_c: {
    borderBottomWidth: 1,
    borderColor: "#23658e",

    backgroundColor: "#276f9c",
    height: 52,

  },
  flex_b_a: {
    backgroundColor: "#266791",
    height: 42,

  },
  flex_b_b: {
    flex: 1,
    color: "#ffffff",
    fontSize: 18,
    height: 42,
    textAlign: "center",
    lineHeight: 32,
  },
  flex_a_c_d: {
    color: "#8db9d5",
    fontSize: 13,
  },
  flex_a_c_c: {
    color: "#ffffff",
    fontSize: 17,
  },
  flex_b_c_b: {
    flex: 1,
    flexDirection: "column",
  },
  flex_a_c_b: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: 'center',
  },
  flex_a_c_a: {
    paddingTop: 10,
    paddingBottom: 10,
    flex: 1,
    height: width / 1.27,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: 'center',
  },
  flex_a_c: {
    backgroundColor: "#2c7eb2",
    flex: 1,

  },
  flex_a_t_c: {
    color: "#ffffff",
    fontSize: 13,
  },
  flex_a_t_b: {
    height: 82,

  },
  flex_a_t_a: {

    fontSize: 34,
    color: "#ffffff",
    height: 82,
    marginTop: 15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  flex_a_t: {
    flexDirection: 'row',
    height: 82,
    borderWidth: 1,
    borderColor: "#2873a2",
    backgroundColor: "#2c7eb2",

  },
  flex_banner_cb: {
    color: "#989898",
    fontSize: 14,
    lineHeight: 25,

  },
  flex_banner_ca: {
    textAlign: "center",
    color: "#63abd9",
    fontSize: 17,
    height: 35,
  },
  flex_banner_c: {
    backgroundColor: "#ffffff",
    flex: 1,
    padding: 22,

  },
  flex_banner_s: {
    height: width / 1.38,
    backgroundColor: "#f00",
  },
  flex_tile_w: {
    height: 48,
    alignItems: 'center',
    color: "#ffffff",
    fontSize: 19,
    lineHeight: 38,
  },
  tabIcon_t: {
    width: 18,
    height: 18,
    resizeMode: 'stretch',
    margin:15
  },
  flex_tile2: {
    flexDirection: 'column',
    alignItems: 'center',
    height: 48,
    flex: 1,
  },
  flex_tile1: {
    width: 48,
    height: 48,

  },
  flex_tile: {
    flexDirection: 'row',
    justifyContent: "flex-start",
    backgroundColor: "#2878aa",
    height: 48,
  },
  flex_t: {
    flex: 1,
    flexDirection: 'column',
  },
  flex: {
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: 'column',

  },
  flex1: {
    position: "relative",
    backgroundColor: '#ffffff',
    height: 50,
    borderTopWidth: 1,
    borderColor: "#dadada",
    flexDirection: 'row',

  },
  tabIcon: {
    width: 25,
    height: 50,
    resizeMode: 'stretch',
  },
  tab: {
    flex: 1,
    height: 50,
    backgroundColor: '#ffffff',
    alignItems: 'center',
  },
  tab_: {
    flex: 1,
    height: 50,
    backgroundColor: '#c6dcea',
    alignItems: 'center',
  },
});


module.exports = HistoryScreen