'use strict'

import RealTimeScreen from './RealTimeScreen'
import HistoryScreen from './HistoryScreen'
import FilterScreen from './FilterScreen'
import SettingScreen from './SettingScreen'

import React, { Component, PropTypes} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text, Platform, Image, BackAndroid,
  ViewPagerAndroid,} from 'react-native';

const COLOR = ['#DFDFDF', 'white']

class MainRoute extends Component {
  static propTypes = {
    navigator: PropTypes.object
  };
  constructor (props) {
    super(props)
    this.state = {
      choice: 0,
    }
  }

  componentWillMount(){
    if (Platform.OS === 'android') {
      BackAndroid.addEventListener('hardwareBackPress', this._goBack.bind(this));
    }
  }

  componentWillUnmount(){
    if (Platform.OS === 'android') {
      BackAndroid.addEventListener('hardwareBackPress', this._goBack.bind(this));
    }
  }

  render () {
    var pages = [];
    var navigator = this.props.navigator
    pages.push(<View key={0}><RealTimeScreen navigator={navigator}/></View>);
    pages.push(<View key={1}><HistoryScreen navigator={navigator}/></View>);
    pages.push(<View key={2}><FilterScreen navigator={navigator}/></View>);
    return (
      <View style={styles.container}>
        <ViewPagerAndroid style={{flex:1}}
          initialPage={0}
          scrollEnabled={false}
          ref={viewPager => { this.viewPager = viewPager; }}>
          {pages}
        </ViewPagerAndroid>
        <View style={styles.Line}/>
        <View style={styles.bottom}>
          <TouchableOpacity style={[styles.bottomButton, {backgroundColor: this.state.choice == 0 ? COLOR[0] : COLOR[1]}]} activeOpacity={1} onPress ={() => this.tabColor(0)}>
            <Image style={styles.buttonImage} source={require('./img/menu_real_time_icon.png')}/>
            <Text style={styles.buttonText}>实时</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.bottomButton, {backgroundColor: this.state.choice == 1 ? COLOR[0] : COLOR[1]}]} activeOpacity={1} onPress ={() => this.tabColor(1)} >
            <Image style={styles.buttonImage}  source={require('./img/menu_history_icon.png')}/>
            <Text style={styles.buttonText}>历史</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.bottomButton, {backgroundColor: this.state.choice == 2 ? COLOR[0] : COLOR[1]}]} activeOpacity={1} onPress ={() => this.tabColor(2)}>
            <Image style={styles.buttonImage} source={require('./img/menu_filter_screen_icon.png')}/>
            <Text style={styles.buttonText}>滤网</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _goBack() {
      const navigator = this.props.navigator;
      if(navigator){
        navigator.pop();
      }
      return true;
  }

  tabColor (num) {
    this.setState({choice: num});
    this.viewPager.setPage(num);
  }
}

var styles = StyleSheet.create({
  container: {
    marginTop: (Platform.OS === 'ios') ? 20 : 0,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  viewShow: {
    flex: 1
  },
  content: {
    flex: 8
  },
  bottom: {
    height: 40,
    backgroundColor: 'black',
    flexDirection: 'row'
  },
  buttonImage: {
    width: 20,
    height: 20,
  },
  buttonText: {
    fontSize: 10
  },
  bottomButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    flexDirection: 'column'
  },
  Icon: {
    color: 'white'
  },
  Line: {
      height: 1,
      backgroundColor: 'gray'
  }
})

module.exports = MainRoute