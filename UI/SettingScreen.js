'use strict'

import React, { Component } from 'react';
import {StyleSheet, View, Text} from 'react-native';

class SettingScreen extends Component {
  render () {
    return (
      <View style={styles.container}>
        <Text>SettingScreen</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});


module.exports = SettingScreen