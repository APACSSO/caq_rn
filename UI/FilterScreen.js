import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  TextInput,
  requireNativeComponent,
  ScrollView,
  TouchableHighlight
} from 'react-native';

var DashboardView = requireNativeComponent('RCTDashboardView', DashboardView);

var Dimensions = require('Dimensions');
var { width, height } = Dimensions.get('window');

class FilterScreen extends Component {

  constructor(props){
    super(props);
    this.state={
      percent:0,
    }
  }

  componentDidMount(){
    this.timer = setInterval(
      () => {
        var tmp = (this.state.percent+5)%100;
        this.setState({percent:tmp});
      },
      1000
    );
  }

  componentWillUnmount(){
    this.timer && clearInterval(this.timer);
  }

  _goBack() {
      const navigator = this.props.navigator;
      if(navigator){
        navigator.pop();
      }
      return true;
  }

  render() {
    return (

      <View style={styles.flex}>
        <View style={styles.flex_t}>
          <View style={styles.flex_tile}>
            <TouchableHighlight style={styles.flex_tile1} onPress={() => this._goBack()}>
              <Image source={require("./img/back_icon.png")} style={styles.tabIcon_t} />
            </TouchableHighlight>
            <View style={styles.flex_tile2}>
              <Text style={styles.flex_tile_w}>滤网情况</Text>
            </View>
            <View style={styles.flex_tile1}>
            </View>
          </View>

          <ScrollView>
            <View style={{ backgroundColor: "#ffffff", height: 60, paddingTop: 30 }}>
              <Text style={styles.flex_banner_ca}>20%建议更换滤网</Text>
            </View>

            <View style={styles.flex_banner_s} >
              <DashboardView style={{ width: 320, height: 320 }} percent={this.state.percent+''} />
            </View>
            <View style={styles.flex_banner_c} >
              <Text style={styles.flex_banner_ca} >
                滤网介绍
            </Text>
              <Text style={styles.flex_banner_cb} >
                汽车空调过滤网是3M公司在中国新上市的产品，传统的汽车过滤网结构致密，不但在使用时会使空调风量骤降，还会在使用较短的时间后即堵塞，造成过滤网的频繁更换。另外，如果所选择的过滤网并不能有效过滤各种固体微粒、有害气体，也就从某种程度上显得如同虚设。在过滤网的选择上，首先要考虑其具有高...
            </Text>
            </View>
          </ScrollView>


        </View>

      </View>
    );
  }
}


const styles = StyleSheet.create({
  flex_banner_cb: {
    color: "#989898",
    fontSize: 14,
    lineHeight: 25,

  },
  flex_banner_ca: {
    textAlign: "center",
    color: "#63abd9",
    fontSize: 17,
    height: 35,
  },
  flex_banner_c: {
    backgroundColor: "#ffffff",
    flex: 1,
    padding: 22,
    marginTop: -120

  },
  flex_banner_s: {
    height: width,
    backgroundColor: "#fff",
    alignItems: "center"
  },
  flex_tile_w: {
    height: 48,
    alignItems: 'center',
    color: "#ffffff",
    fontSize: 19,
    lineHeight: 38,
  },
  tabIcon_t: {
    width: 18,
    height: 18,
    resizeMode: 'stretch',
    margin:15
  },
  flex_tile2: {
    flexDirection: 'column',
    alignItems: 'center',
    height: 48,
    flex: 1,
  },
  flex_tile1: {
    width: 48,
    height: 48,

  },
  flex_tile: {
    flexDirection: 'row',
    justifyContent: "flex-start",
    backgroundColor: "#2878aa",
    height: 48,
  },
  flex_t: {
    flex: 1,
    flexDirection: 'column',
  },
  flex: {
    backgroundColor: "#000",
    flex: 1,
    flexDirection: 'column',

  },
  flex1: {
    position: "relative",
    backgroundColor: '#ffffff',
    height: 50,
    borderTopWidth: 1,
    borderColor: "#dadada",
    flexDirection: 'row',

  },
  tabIcon: {
    width: 25,
    height: 50,
    resizeMode: 'stretch',
  },
  tab: {
    flex: 1,
    height: 50,
    backgroundColor: '#ffffff',
    alignItems: 'center',
  },
  tab_: {
    flex: 1,
    height: 50,
    backgroundColor: '#c6dcea',
    alignItems: 'center',
  },
});


module.exports = FilterScreen